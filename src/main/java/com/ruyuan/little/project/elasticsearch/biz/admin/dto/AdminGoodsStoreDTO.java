package com.ruyuan.little.project.elasticsearch.biz.admin.dto;

import lombok.Data;

/**
 * @author <a href="mailto:little@163.com">little</a>
 * version: 1.0
 * Description:后台店铺分组查询dto实体
 **/
@Data
public class AdminGoodsStoreDTO {

    /**
     * 店铺名称
     */
    private String  storeName;

    /**
     * 页码
     */
    private Integer page;

    /**
     * 每页大小
     */
    private Integer size;

}
